﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathLibrary
{
    public class Matrix3
    {
        /* PROPERTIES */
        public float m1, m2, m3;
        public float m4, m5, m6;
        public float m7, m8, m9;

        /* CONSTRUCTORS */
        public Matrix3()
        {
            m1 = 0; m2 = 0; m3 = 0;
            m4 = 0; m5 = 0; m6 = 0;
            m7 = 0; m8 = 0; m9 = 0;
        }

        public Matrix3(float m1, float m2, float m3,
                       float m4, float m5, float m6,
                       float m7, float m8, float m9)
        {
            this.m1 = m1; this.m2 = m2; this.m3 = m3;
            this.m4 = m4; this.m5 = m5; this.m6 = m6;
            this.m7 = m7; this.m8 = m8; this.m9 = m9;
        }

        /* METHODS */
        /// <summary>
        /// Multiples 2 matrix3's together
        /// </summary>
        /// <param name="a">First Matrix</param>
        /// <param name="b">Second Matrix</param>
        /// <returns></returns>
        public static Matrix3 operator *(Matrix3 a, Matrix3 b)
        {
            return new Matrix3((a.m1 * b.m1), (a.m4 * b.m2), (a.m7 * b.m3),
                               (a.m2 * b.m4), (a.m5 * b.m5), (a.m8 * b.m6),
                               (a.m3 * b.m7), (a.m6 * b.m8), (a.m9 * b.m9));
        }
    }
}
