﻿using System;

namespace MathLibrary
{
    public class Vector3
    {
        /* PROPERTIES */
        public float x, y, z;

        /* CONSTRUCTORS */
        public Vector3()
        {
            x = 0;
            y = 0;
            z = 0;
        }

        public Vector3(float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public Vector3(int x, int y, int z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        /* METHODS */

        /// <summary>
        /// Adds the 2 Vectors together
        /// </summary>
        /// <param name="v1">First Vector</param>
        /// <param name="v2">Second Vector</param>
        /// <returns>The 2 vectors added together</returns>
        public static Vector3 operator +(Vector3 v1, Vector3 v2)
        {
            return new Vector3((v1.x + v2.x), (v1.y + v2.y), (v1.z + v2.z));
        }

        /// <summary>
        /// Substracts vector 2 from vector 1
        /// </summary>
        /// <param name="v1">First Vector</param>
        /// <param name="v2">Second Vector</param>
        /// <returns>Vector 1 subtracted by Vector 2</returns>
        public static Vector3 operator -(Vector3 v1, Vector3 v2)
        {
            return new Vector3((v1.x - v2.x), (v1.y - v2.y), (v1.z - v2.y));
        }

        /// <summary>
        /// Multiplies the vector by a scale
        /// </summary>
        /// <param name="v1">First Vector</param>
        /// <param name="scale">Float to scale the vector</param>
        /// <returns>First Vector Multiplied by scale</returns>
        public static Vector3 operator *(Vector3 v1, float scale)
        {
            return new Vector3((v1.x * scale), (v1.y * scale), (v1.z * scale));
        }

        /// <summary>
        /// Multiplies the vector by a scale
        /// </summary>
        /// <param name="v1">First Vector</param>
        /// <param name="scale">Float to scale the vector</param>
        /// <returns>First Vector Multiplied by scale</returns>
        public static Vector3 operator *(float scale, Vector3 v1)
        {
            return new Vector3((v1.x * scale), (v1.y * scale), (v1.z * scale));
        }

        /// <summary>
        /// Multiples Matrix 3 by a vector
        /// </summary>
        /// <param name="mat"></param>
        /// <param name="vec"></param>
        /// <returns>New Vector from a matrix</returns>
        public static Vector3 operator *(Matrix3 mat, Vector3 vec)
        {
            return new Vector3((mat.m1 * vec.x) + (mat.m2 * vec.y) + (mat.m3 * vec.z),
                                (mat.m4 * vec.x) + (mat.m5 * vec.y) + (mat.m5 + vec.z),
                                (mat.m7 * vec.x) + (mat.m8 * vec.y) + (mat.m9 * vec.z));
        }

        /// <summary>
        /// Multiples Matrix 3 by a vector
        /// </summary>
        /// <param name="mat"></param>
        /// <param name="vec"></param>
        /// <returns>New Vector from a matrix</returns>
        public static Vector3 operator *(Vector3 vec, Matrix3 mat)
        {
            return new Vector3((mat.m1 * vec.x) + (mat.m2 * vec.y) + (mat.m3 * vec.z),
                                (mat.m4 * vec.x) + (mat.m5 * vec.y) + (mat.m5 + vec.z),
                                (mat.m7 * vec.x) + (mat.m8 * vec.y) + (mat.m9 * vec.z));
        }

        // TODO: Dot
        // TODO: Cross
        // TODO: Matrix3 
        // TODO: Matrix4
        // TODO: Normalize
        // TODO: Magnitude

    }
}
