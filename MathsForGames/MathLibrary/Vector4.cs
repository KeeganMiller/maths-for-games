﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathLibrary
{
    public class Vector4
    {
        /* PROPERTIES */
        public float x, y, z, w;

        /*  CONSTRUCTORS */
        public Vector4()
        {
            x = 0;
            y = 0;
            z = 0;
            w = 1;
        }

        public Vector4(float x, float y, float z, float w)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            this.w = w;
        }

        /* METHODS */

        /// <summary>
        /// Adds the 2 Vectors together
        /// </summary>
        /// <param name="v1">First Vecotr</param>
        /// <param name="v2">Second Vector</param>
        /// <returns>v1 with v2 axis combined</returns>
        public static Vector4 operator +(Vector4 v1, Vector4 v2)
        {
            return new Vector4((v1.x + v2.x), (v1.y + v2.y), (v1.z + v2.z), (v1.w + v2.w));
        }

        /// <summary>
        /// Substracts the 2 vectors
        /// </summary>
        /// <param name="v1">First Vecotr</param>
        /// <param name="v2">Second Vector</param>
        /// <returns>v1 subtract v2</returns>
        public static Vector4 operator -(Vector4 v1, Vector4 v2)
        {
            return new Vector4((v1.x - v2.x), (v1.y - v2.y), (v1.z - v2.z), (v1.w - v2.w));
        }

        /// <summary>
        /// Multiplies the Vector 4 by a scale
        /// </summary>
        /// <param name="v1">First Vector</param>
        /// <param name="scale">Scale to multiple by</param>
        /// <returns>The vector multiplied by the scale</returns>
        public static Vector4 operator *(Vector4 v1, float scale)
        {
            return new Vector4((v1.x * scale), (v1.y * scale), (v1.z * scale), (v1.w * scale));
        }

        /// <summary>
        /// Multiplies the Vector 4 by a scale
        /// </summary>
        /// <param name="v1">First Vector</param>
        /// <param name="scale">Scale to multiple by</param>
        /// <returns>The vector multiplied by the scale</returns>
        public static Vector4 operator *(float scale, Vector4 v1)
        {
            return new Vector4((v1.x * scale), (v1.y * scale), (v1.z * scale), (v1.w * scale));
        }

        // TODO: Dot
        // TODO: Cross
        // TODO: Matrix3 
        // TODO: Matrix4
        // TODO: Normalize
        // TODO: Magnitude

    }
}
